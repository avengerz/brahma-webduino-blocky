Blockly.Blocks['wificubeid'] = {
      init: function() {
        this.appendDummyInput()
            .appendField("WiFi Cube  |  ID")
            .appendField(new Blockly.FieldTextInput(""), "value_");
        this.setOutput(true, null);
        this.setColour(285);
        this.setTooltip("");
        this.setHelpUrl("");
      }
    };