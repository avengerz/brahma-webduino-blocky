Blockly.JavaScript['scratchubportvalue'] = function(block) {
  var dropdown_scratchubvalue = block.getFieldValue('ScratcHubValue');
  var value_portvalue = Blockly.JavaScript.valueToCode(block, 'PortValue', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'getport(' + 'board ,' +  dropdown_scratchubvalue + ');';
  // return code;
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};