Blockly.Blocks['scratchubportvalue'] = {
  init: function() {
    this.appendValueInput("PortValue")
        .setCheck(null)
        .appendField("Set Port")
        .appendField(new Blockly.FieldDropdown([
                  ["1","1"],
                  ["2","2"],
                  ["3","3"],
                  ["4","4"],
                  ["5","5"],
                  ["6","6"],
                  ["7","7"],
                  ["8","8"]
                ]), "ScratcHubValue")
        .appendField("as");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};